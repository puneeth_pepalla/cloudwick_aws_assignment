import io
import re
import boto3
import json

s3=boto3.client("s3")
def lambda_handler(event, context):
    bucket=event['Records'][0]['s3']['bucket']['name']
    file=event['Records'][0]['s3']['object']['key']
    det=file.split(".")[0].split("-")
    uname=det[0]
    dname=det[1]
    response = s3.get_object(Bucket=bucket, Key=file)
    #red=io.BytesIO(response['Body'].read())
    csv_file = response['Body'].read().decode('utf-8')
    csv_lines = csv_file.split("\r\n")
    header = csv_lines[0].split(",")
    first_row = csv_lines[1].split(",")
    new_dict = {}
    for i in range(0,len(first_row)):
        new_dict[header[i]] = schema(first_row[i])
    #print(new_dict)
    table = boto3.resource('dynamodb').Table('datasets')
    table.update_item(
    Key={'datasetname': dname},
    UpdateExpression="set datschema = :r",
    ExpressionAttributeValues={
        ':r': new_dict
    })
    js=json.dumps(new_dict)
    return new_dict
	
def schema(word):
	word = str(word)
	if re.match('([0-9]+)+(\.[0-9]+)', word):
		return "float"
	elif re.match('[0-9]+', word):
		return "int"
	else:
		return "string"  