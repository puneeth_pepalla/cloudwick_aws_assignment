import re
import json
import traceback
import boto3
from boto3.dynamodb.conditions import Key, Attr

table_name = 'Students'

dynamodb_client = boto3.client('dynamodb')
#table = dynamodb.Table('Students')
def lambda_handler(event, context):
  if event['path']=="/":    
    if event['method']=="POST" :
        dynamodb_client.put_item(TableName=table_name, Item={
            'studentName':{'S':str(event['body']['name'])},
            'studentEmail':{'S':str(event['body']['email'])},
            'password':{'S':str(event['body']['pwd'])}
        })
        
        print ("PutItem succeeded:")
        #print(json.dumps(response, indent=4, cls=DecimalEncoder))
        return "put succeeded"
    
    else:
        name=event['name']
        #name=event.queryStringParameters.studentName
        #name=event['params']['querystring']['student']
        item=dynamodb_client.get_item(TableName=table_name, Key={'studentName':{'S':name}})
        
        if('Item' not in item) :
            return "not found"
        return item['Item']
        
  if event['path']=="/dataset":
      #table_name="datasets"
      if event['method']=="POST" :
        dynamodb_client.put_item(TableName="datasets", Item={
            'user':{'S':str(event['body']['user'])},
            'datasetname':{'S':str(event['body']['datasetname'])},
            'description':{'S':str(event['body']['description'])},
            'domain':{'S':str(event['body']['domain'])},
            'privated':{'S':str(event['body']['privated'])},
            'visibility':{'S':str(event['body']['visibility'])},
            'dstatus':{'S': 'pending'}
         
        })
        return "put succeeded"
      else:
        dynamodb = boto3.resource('dynamodb')

        table = dynamodb.Table('datasets')
        
        response = table.scan(
            FilterExpression=Attr('user').eq(event['name'])
        )
        return response['Items']
        
  if event['path']=="/schma":
      #table_name="datasets"
      if event['method']=="POST" :
        table = boto3.resource('dynamodb').Table('datasets')
        table.update_item(
        Key={'datasetname': event['body']['dname']},
        UpdateExpression="set datschema = :r, dstatus= :s",
        ExpressionAttributeValues={
            ':r': event['body']['schma'],
            ':s': "published"
        })
          
        # dynamodb_client.put_item(TableName="datasets", Item={
        #     'user':{'S':str(event['body']['user'])},
        #     'datasetname':{'S':str(event['body']['datasetname'])},
        #     'description':{'S':str(event['body']['description'])},
        #     'domain':{'S':str(event['body']['domain'])},
        #     'privated':{'S':str(event['body']['privated'])},
        #     'visibility':{'S':str(event['body']['visibility'])},
        #     'status':{'S': 'published'}
         
        # })
        return "put succeeded"
      else:
        dynamodb = boto3.resource('dynamodb')

        table = dynamodb.Table('datasets')
        
        response = table.scan(
            FilterExpression=Attr('datasetname').eq(event['name'])
        )
        return response['Items']
        
  if event['path']=="/search":
        dynamodb = boto3.resource('dynamodb')

        table = dynamodb.Table('datasets')
        
        response = table.scan()
            #FilterExpression=Attr('datasetname').eq(event['name'])
        results=[]
        for item in response['Items']:
            item['privated']='classified'
            for key in item.keys():
                if(event['name']==item[key] and item['dstatus']=='published'):
                   
                    results.append(item)
        
        if(event['name']=='*'):
            return response['Items']
        
        else:
            return results        

        
      