import io
import json
import re
import boto3
import pandas as pd
import numpy as np
s3=boto3.client("s3")
def lambda_handler(event, context):
    bucket=event['Records'][0]['s3']['bucket']['name']
    file=event['Records'][0]['s3']['object']['key']
    det=file.split(".")[0].split("-")
    uname=det[0]
    dname=det[1]
    response = s3.get_object(Bucket=bucket, Key=file)
    df = pd.read_csv(io.BytesIO(response['Body'].read()))
    df2=df.iloc[0]
    print("First row:")
    print(df2)
    new_dict={}
    new_list=df.columns.values.tolist()
    i=0
    print("columns list:")	
    print(new_list)
    for word in new_list:
        a=df2[word]
        new_dict[word]='String'
        i=i+1
    # TODO implement
    #print(new_dict)
    table = boto3.resource('dynamodb').Table('datasets')
    table.update_item(
    Key={'datasetname': dname},
    UpdateExpression="set datschema = :r",
    ExpressionAttributeValues={
        ':r': new_dict
    })
    #js=json.dumps(new_dict)
    return new_dict
	
def schema(word):
    number=re.compile(r'\d+')
    if np.issubdtype(word, np.integer):
        return 'Integer'
    else:
        return 'String'